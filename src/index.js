import axios from 'axios';

require('dotenv').config();

const USERNAME = process.env.USERNAME;
const PASSWORD = process.env.PASSWORD;

const namesOfIntegrationsToDelete = ['Connors-AEM-Bot', 'Automation-Demo-Walmart', 'Walmart-Automation-Demo']; //Populate with the names of your old integrations

const getApiKey = async () => {
    const res = await axios.put(`https://hoodoodigital.my.workfront.com/attask/api/v9.0/user?action=generateApiKey&username=${USERNAME}&password=${PASSWORD}&method=put`);
    const apiKey = res.data.data.result;
    return apiKey;
}

const getAllCustomIntegrations = async (apiKey) => {
    const endpoint = `https://hoodoodigital.my.workfront.com/attask/api-unsupported/DOCCFG/search?fields=*&$$LIMIT=-1`;
    const method = 'get';
    const res = await apiCall(endpoint, apiKey, method)
    return res.data.data;
}

const getCustomProvider = async (id, apiKey) => {
    const endpoint = `https://hoodoodigital.my.workfront.com/attask/api-unsupported/DOCPRO/search?docProviderConfigID=${id}&fields=*`;
    const method = 'get';
    const res = await apiCall(endpoint, apiKey, method)
    console.log("getCustomProvider:",res.data)
    return res.data;
}

const getCustomIntegration = async (id, apiKey) => {
    const endpoint = `https://hoodoodigital.my.workfront.com/attask/api-unsupported/DOCCFG/search?ID=${id}&fields=*`;
    const method = 'get';
    const res = await apiCall(endpoint, apiKey, method)
    console.log(res.data)
}

const deleteCustomProvider = async (id, apiKey) => {
    const endpoint = `https://hoodoodigital.my.workfront.com/attask/api-unsupported/DOCPRO/${id}`;
    const method = 'delete';
    const res = await apiCall(endpoint, apiKey, method);
    console.log(res.data)
}

const deleteCustomIntegration = async (id, apiKey) => {
    const endpoint = `https://hoodoodigital.my.workfront.com/attask/api-unsupported/DOCCFG/${id}`;
    const method = 'delete';
    const res = await apiCall(endpoint, apiKey, method)
    console.log(res.data)
}

const apiCall = async (endpoint, apiKey, method) => {
    let config = {
        method: method,
        url: endpoint,
        headers: {
            'ApiKey': apiKey
        }
    }
    const res = await axios(config);
    return res;
}

(async ()=> {
    const apiKey = await getApiKey();
    //get custom integrations
    const customIntegrations = await getAllCustomIntegrations(apiKey);
    //filter custom integrations to Connors-AEM-Bot and aren't active
    const customIntegrationsToBeDeleted = customIntegrations.filter(customIntegration => {
        return (namesOfIntegrationsToDelete.includes(customIntegration.name) && !customIntegration.isActive);
    })
    console.log(customIntegrationsToBeDeleted)
    //grab the ID's
    const customIntegrationIds = customIntegrationsToBeDeleted.map(
        integration => integration.ID
    )
    console.log(customIntegrationIds)
    //get provider by ID, store provider ID
    customIntegrationIds.forEach(async (id) => {
        console.log("ID: ", id)
        const customProviderRes = await getCustomProvider(id, apiKey)
        console.log('customProviderRes', customProviderRes.data)
        if(customProviderRes.data.length) {
            const customProviderId = await customProviderRes.data[0].ID;
            await deleteCustomProvider(customProviderId, apiKey)
        } 
        await deleteCustomIntegration(id, apiKey);
    });
})();


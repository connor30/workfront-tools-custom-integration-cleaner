# Workfront Custom Integration Clean Up
This Node-JS script will automatically clean up your old workfront integrations.

## Setup
### npm
Start by entering the root folder and typing on your console `npm install`. This will download the necassary packages the code will need to compile.  

### .env file
Start by creating a file within the root folder of the project. This file should be called ".env".  
The contents of the file should be:  
```
USERNAME=<workfrontUsername>
PASSWORD=<workfrontPassword>
```

### setting up the base code file
In ./src/index.js, you should change the variable `namesOfIntegrationsToDelete` so that it is a list of the integrations you want to get rid of.  
For Example:  
`const namesOfintegrationsToDelete = ["myWorkfrontInt", "WorkfrontConnectorHoodoo"]`

## Commands
`npm start` - will start a server that will restart with every saved change and compiles with babel  
`npm run-script build` - will compile the code with babel - use this only if you need to distribute the code or it is running slow  
`npm run-script serve` - will run the compiled code
 
The code may take a couple of runs before it cleans up all the integrations. Unfortunately results are paginated so we can't get all the integrations with one run